from __future__ import division
import argparse
import os
import re
import io
from PIL import Image
from bs4 import BeautifulSoup
import base64



parser = argparse.ArgumentParser(description="Styling Ketos results.")
parser.add_argument('-i', '--input', action='store', nargs=1, required=True, help='Path to directory with files to transform')
args = parser.parse_args()

regex_1 = r"(height:)(.+)(%;)"
subst_1 = "\\1 1.3\\3"

export_directory = os.path.join(args.input[0], 'transformed')
if not os.path.exists(export_directory):
    os.makedirs(export_directory)

for (i,file)  in enumerate(os.listdir(args.input[0])):
    check = False
    if file.endswith('.html'):
        print('************************************************************')
        with open(os.path.join(args.input[0], file), 'r') as fh:
            print(file)
            print(type(fh))
            xml_content = fh.read()
        parsed_html = BeautifulSoup(xml_content, 'html.parser')
        imgdata = base64.b64decode(parsed_html.img['src'].split(',')[1])
        im = Image.open(io.BytesIO(imgdata))
        width, height = im.size
        print(im.size)
        second_div = parsed_html.findAll("div", {"class": "column"})
        div_of_li = second_div[len(second_div) - 1]
        style_div = 'width:'+str( width/3) + 'px; height:'+ str(height/3) +'px; position:absolute'
        div_of_li.attrs['style']= style_div
        for (href, elt) in zip(parsed_html.find_all('a', {"class" : "rect"}), parsed_html.find_all('li',{"contenteditable" : "true"} )):
            #print('link:', href, 'and elt :' ,elt)
            if(href.get('alt') == elt.get('id')):
                #print(href.get('alt'), '== ', elt.get('id'))
                elt.attrs['style']=  re.sub(regex_1, subst_1, href.get("style"), 0, re.MULTILINE)
                #print(elt)
                check = True
            else: 
                print(href.get('alt'), '## ', elt.get('id'))
        if (check == True):
            with open(os.path.join(export_directory, file.replace('.html', '_transformed.html')), 'w') as fh:
                fh.write(str(parsed_html))
            
print("[FINI]: Et voilà Alix !")