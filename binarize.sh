#!/bin/bash

# lance la binarization avec kraken pour tous les fichiers .jpg présents dans le dossier

for file in ./*.jpg ; do
	kraken -i  "$(basename "$file")" "${file}.png" binarize
done
